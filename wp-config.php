<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bestwebhosting');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5DrY&:vf,Y?<cwFN[?zW$;OCS3Xv!psj W@eIV(}G-u_;#3/O*])iL_ 3yI8E+ns');
define('SECURE_AUTH_KEY',  'S#HJHy|oLA?Sz$*>rA;^YFk9|v<:.F1vQ[>K&6~K`O;k5dMG.uEmOh@M_ gP(5w)');
define('LOGGED_IN_KEY',    'Sw1t.9Kuky$h?%f@oihR _&-w@~*H^!T)?&lLz [M!+kMtH>cq;>Ge({NTY^fT`o');
define('NONCE_KEY',        '=dJQDGkC2NoIuFhR(qZAqW-`leuzgt%YLkTN$:UY+Yy<Doqf5^smIUy@b~pG[{$_');
define('AUTH_SALT',        'Ns]`JdYNBSn)jYnNsy[%Xm~M)TQE_;DBHLQ,J{4AXXey^HUm4>^qMYMYC6/?S[7o');
define('SECURE_AUTH_SALT', 'Pon ERDa#na~+QtSE[r4O@9ahU&Z$OQjV]GY%A#c[f;Q}.&B~O6_Yc,{{v<1 (Cs');
define('LOGGED_IN_SALT',   '$%%`RLH^{3h1)TF{@J7r(+s<U9%Gb[ wY|2Z%nmfu7w VVWNvnqBePXS:f{$0=JM');
define('NONCE_SALT',       '{<H;!rbXlwH>Az.:SAE.<:!lS(OQOoH215ORVENu$7n0VN/ST|;-oLG+vHSXXFy<');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
