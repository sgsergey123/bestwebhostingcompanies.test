<?php

add_theme_support('custom-logo');
register_nav_menu('primary', 'Header Menu');
add_theme_support( 'post-thumbnails', array( 'hosting' ) );

add_action( 'wp_enqueue_scripts', 'theme_style' );

add_action( 'wp_enqueue_scripts', 'theme_script' );

add_action( 'init', 'add_hosting');

function theme_style () {
    wp_enqueue_style('bootstrap.min.css',get_template_directory_uri()."/css/bootstrap.min.css");
    wp_enqueue_style('css/main.css',get_template_directory_uri()."/css/main.css");
}

function theme_script() {
    wp_enqueue_script( 'js/jquery.min.js', get_template_directory_uri().'/js/jquery.min.js', array(), '', true );
    wp_enqueue_script( 'js/bootstrap.min.js', get_template_directory_uri().'/js/bootstrap.min.js', array(), '', true );
}

function add_hosting(){

    $lables = array(

        'name' => 'Hosting',

        'singular_name' => 'Hosting',

        'add_new' => 'Add Item',

        'all_items' => 'All items',

        'add_new_item' => 'Add item',

        'edit_item' => 'Edit item',

        'new_item' => 'New item',

        'view_item' => 'View item',

        'search_items' => 'Search Hosting',

        'not_found' => 'No item found',

        'not_found_in_trash' => 'No item found in trash',

        'parent_item_colon' => 'Parent item',

        'menu_name' => 'Hosting'

    );

    $args = array(

        'labels' => $lables,

        'public' => true,

        'has_archive' => 'hosting',

        'publicly_queryable' => true,

        'query_var' => true,

        'rewrite' => true,

        'capability_type' => 'post',

        'hierarchical' => false,

        'supports' => array('title','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'),

        'taxonomies' => array('category', 'post_tag'),

        'menu_position' => 2,

        'exclude_from_search' => false

    );

    register_post_type('hosting', $args);

}