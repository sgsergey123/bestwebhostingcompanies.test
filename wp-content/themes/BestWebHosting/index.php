<?php get_header(); ?>

        <div class="container main_content">
            <h1>The Most Popular Price Plan</h1>
            <div class="row info-box">
                <div class="col-sm-2 col-xs-12">
                    <img src="img/bluehost.png" alt="bluehost">
                    <p class="stars">
                        <i class="glyphicon glyphicon-star" aria-hidden="true"></i>
                        <i class="glyphicon glyphicon-star" aria-hidden="true"></i>
                        <i class="glyphicon glyphicon-star" aria-hidden="true"></i>
                        <i class="glyphicon glyphicon-star" aria-hidden="true"></i>
                        <i class="glyphicon glyphicon-star" aria-hidden="true"></i>
                    </p>
                    <p>5/5 Rating</p>
                </div>
                <div class="col-sm-2  col-xs-6">
                    <p><span>$2.95</span> /month</p>
                </div>
                <div class="col-sm-1  col-xs-6 cloud">
                    <i class="glyphicon glyphicon-cloud" aria-hidden="true"></i>
                    <p>50 GB space</p>
                </div>
                <div class="col-sm-2  col-xs-6">
                    <i class="glyphicon glyphicon-transfer" aria-hidden="true"></i>
                    <p>Unlimited bandwidth</p>
                </div>
                <div class="col-sm-2  col-xs-6">
                    <i class="glyphicon glyphicon-open" aria-hidden="true"></i>
                    <p>99.99% Uptime</p>
                </div>
                <div class="col-sm-3  col-xs-12 buttons">
                    <a href="#">Go to Bluehost.com</a>
                    <a href="#">Go to Review</a>
                </div>
            </div>

            <div class="row info-box">
                <div class="col-sm-2 col-xs-12">
                    <img src="img/bluehost.png" alt="bluehost">
                    <p class="stars">
                        <i class="glyphicon glyphicon-star" aria-hidden="true"></i>
                        <i class="glyphicon glyphicon-star" aria-hidden="true"></i>
                        <i class="glyphicon glyphicon-star" aria-hidden="true"></i>
                        <i class="glyphicon glyphicon-star" aria-hidden="true"></i>
                        <i class="glyphicon glyphicon-star" aria-hidden="true"></i>
                    </p>
                    <p>5/5 Rating</p>
                </div>
                <div class="col-sm-2  col-xs-6">
                    <p><span>$2.95</span> /month</p>
                </div>
                <div class="col-sm-1  col-xs-6 cloud">
                    <i class="glyphicon glyphicon-cloud" aria-hidden="true"></i>
                    <p>50 GB space</p>
                </div>
                <div class="col-sm-2  col-xs-6">
                    <i class="glyphicon glyphicon-transfer" aria-hidden="true"></i>
                    <p>Unlimited bandwidth</p>
                </div>
                <div class="col-sm-2  col-xs-6">
                    <i class="glyphicon glyphicon-open" aria-hidden="true"></i>
                    <p>99.99% Uptime</p>
                </div>
                <div class="col-sm-3  col-xs-12 buttons">
                    <a href="#">Go to Bluehost.com</a>
                    <a href="#">Go to Review</a>
                </div>
            </div>

            <div class="row info-box">
                <div class="col-sm-2 col-xs-12">
                    <img src="img/bluehost.png" alt="bluehost">
                    <p class="stars">
                        <i class="glyphicon glyphicon-star" aria-hidden="true"></i>
                        <i class="glyphicon glyphicon-star" aria-hidden="true"></i>
                        <i class="glyphicon glyphicon-star" aria-hidden="true"></i>
                        <i class="glyphicon glyphicon-star" aria-hidden="true"></i>
                        <i class="glyphicon glyphicon-star" aria-hidden="true"></i>
                    </p>
                    <p>5/5 Rating</p>
                </div>
                <div class="col-sm-2  col-xs-6">
                    <p><span>$2.95</span> /month</p>
                </div>
                <div class="col-sm-1  col-xs-6 cloud">
                    <i class="glyphicon glyphicon-cloud" aria-hidden="true"></i>
                    <p>50 GB space</p>
                </div>
                <div class="col-sm-2  col-xs-6">
                    <i class="glyphicon glyphicon-transfer" aria-hidden="true"></i>
                    <p>Unlimited bandwidth</p>
                </div>
                <div class="col-sm-2  col-xs-6">
                    <i class="glyphicon glyphicon-open" aria-hidden="true"></i>
                    <p>99.99% Uptime</p>
                </div>
                <div class="col-sm-3  col-xs-12 buttons">
                    <a href="#">Go to Bluehost.com</a>
                    <a href="#">Go to Review</a>
                </div>
            </div>
        </div>

        <div class="containet-fluid comments_section">
            <div class="container">
                <div class="row comment-header">
                    <div class="col-xs-10">
                        <h1>Comments <span>51</span></h1>
                    </div>
                    <div class="col-xs-2 text-right ">
                        <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>
                    </div>
                </div>

                <div class="row comment-body">
                    <div class="col-sm-offset-1 col-sm-11 col-xs-12">
                        <section>
                            <h2>ANDY BRIAN <span>April 5, 2017</span></h2>
                            <p>Seriously its an amazing guide about web hosting. Web hosting is an integral part of every major business indeed. Business owners should consider all these factors before choosing web hosting services. How much space you will need? Should be the question in mind. How many email--addresses and domains you have required is also essential to determine. Security level is another important factor that should be considered by one.</p>
                            <div><i class="glyphicon glyphicon-comment" aria-hidden="true"></i> REPLY</div>
                        </section>
                    </div>
                </div>

                <div class="row reply-body">
                    <div class="col-sm-offset-2 col-sm-10 col-xs-offset-1 col-xs-11">
                        <section>
                            <h2>ANDY BRIAN <span>April 5, 2017</span></h2>
                            <p>Seriously its an amazing guide about web hosting. Web hosting is an integral part of every major business indeed. Business owners should consider all these factors before choosing web hosting services. How much space you will need? Should be the question in mind. How many email--addresses and domains you have required is also essential to determine. Security level is another important factor that should be considered by one.</p>
                            <div><i class="glyphicon glyphicon-comment" aria-hidden="true"></i> REPLY</div>
                        </section>
                    </div>
                </div>


                <div class="row submit-comment">
                    <div class="col-xs-12">
                        <section>
                            <h3>Submit a Comment</h3>
                            <form action="" method="" role="form">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label for="text">MESSAGE</label>
                                            <textarea name="text" class="col-xs-12"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-4 col-xs-12">
                                            <label for="username">Name</label>
                                            <input type="text" name="username" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-4  col-xs-12">
                                            <label for="email">Email</label>
                                            <input type="email" name="email" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-3  col-xs-12">
                                            <input type="submit" name="submit" class="form-control" value="POST COMMENT">
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </section>
                    </div>
                </div>


            </div>
        </div>
    </main>
    <footer class="contaner-fluid text-center">
        <h5>© 2017 All rights reserved</h5>
    </footer>
<?php get_footer(); ?>